# Prplprobe Memory Collector

Prplprobe is a monitoring system designed to offer key performance indicators (KPIs) about the system (CPU, Memory, Wi-Fi, etc).

## Table of Contents

[[_TOC_]]

## Introduction

`prplprobe-memory-collector` is a service used to collect KPIs about memory. For now the following events are implemented:
- Global
- Leak
- Top10

## Installation

### Prerequisites

The follwoing packages are required to build `prplprobe-memory-collector`:

- CMake

### Dependencies

`prplprobe-memory-collector` relies on the following dependencies:

- Protobuf (https://github.com/protocolbuffers/protobuf)
- gRPC (https://github.com/grpc/grpc)
- libsahtrace (https://gitlab.com/prpl-foundation/components/core/libraries/libsahtrace)
- libprplprobe (https://gitlab.com/prpl-foundation/components/prplprobe/libprplprobe)

### Build from source

1. **Clone the repository**:

```sh
git clone https://gitlab.com/prpl-foundation/components/prplprobe/prplprobe-memory-collector.git
cd prplprobe-memory-collector
```

2. **Build the service**

```sh
mkdir build
cd build
cmake ..
make
make install
```

## Documentation

### Prerequisites

The follwoing packages are required to build `prplprobe-memory-collector` documentation:

- Doxygen
- Graphviz

### Build doc

Both protobuf and C++ APIs documentations are generated in the same way:

```sh
cmake .. -DGEN_DOC=y
make
make install
```

## Configuration

### Build-time configuration

During the build process, you can customize `prplprobe-memory-collector` by setting specific configuration options to meet your project's requirements. Below are the available build configuration options:

- **`CONFIG_SAH_PRPLPROBE_MEMORY_COLLECTOR_PROC_PREFIX_PATH`**: Path where we look for /proc/ dir, this is mainly used for containers, full path searched is CONFIG_SAH_PRPLPROBE_MEMORY_COLLECTOR_PROC_PREFIX_PATH/proc/.

- **`GEN_DOC`**: Enabling this option generates documentation for `prplprobe-memory-collector`. Make sure to set this to `y` to generate documentation during the build process.

- **`COVERAGE`**: If you wish to include coverage flags and run tests with coverage analysis, enable this option by setting it to `y`.

To configure these options during the build process, you can set them in cmake command directly or use your buildsystem options.

## Run-time Configuration

`prplprobe-memory-collector` is configured using a JSON-formatted protobuf message in a configuration file. Below is an example of the configuration file structure:

```json
{
    "sahtrace_config": {
        "sah_trace": {
            "type": "SYSLOG",
            "level": 200
        },
        "trace_zones": [
            {
                "name": "all",
                "level": 200
            }
        ]
    }
}
```

In this example configuration, you can see the structure for specifying sahtrace configuration. Users can modify this JSON file to customize the `prplprobe-memory-collector`'s behavior according to their requirements.

## Testing

### Prerequisites

The follwoing packages are required to build `prplprobe-memory-collector` documentation:
- gcovr
- valgrind

### Build and run tests

```sh
cmake .. -DCOVERAGE=y
make
```

## License

`prplprobe-memory-collector` is licensed under the [BSD-2-Clause-Patent](https://spdx.org/licenses/BSD-2-Clause-Patent.html) license.

You can find a copy of the license in the [LICENSE](./LICENSE) file.

Please review the license carefully before using or contributing to this project. By participating in the `prplprobe-memory-collector` community, you agree to adhere to the terms and conditions set forth in this license.

For more details about the license and its implications, refer to the [full license text](https://spdx.org/licenses/BSD-2-Clause-Patent.html).
