#!/bin/sh

[ -f /etc/environment ] && source /etc/environment

ulimit -c ${ULIMIT_CONFIGURATION:-0}

start() {
    /usr/lib/prplprobe/collectors/prplprobe-memory-collector
}

stop() {
    kill $(pidof prplprobe-memory-collector)
}

case $1 in
    start|boot)
        start
        ;;
    stop|shutdown)
        stop
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        echo "TODO debuginfo prplprobe-memory-collector"
        ;;
    log)
        echo "TODO log prplprobe-memory-collector"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|restart|debuginfo|log]"
        ;;
esac
