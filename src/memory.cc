/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS AS IS AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <cstring>
#include <string>

#include <dirent.h>
#include <sys/stat.h>

#include <debug/sahtrace.h>

#include <prplprobe/common.pb.h>
#include <prplprobe/memory_global.pb.h>
#include <prplprobe/memory_leak.pb.h>
#include <prplprobe/memory_top10.pb.h>
#include <prplprobe/module.pb.h>

#include <prplprobe/client_utils.h>
#include <prplprobe/config_utils.h>
#include <prplprobe/event_loop.h>
#include <prplprobe/func_utils.h>
#include <prplprobe/grpc_utils.h>
#include <prplprobe/sahtrace_utils.h>

#include "memory.h"

#define PROC_DIR CONFIG_SAH_PRPLPROBE_MEMORY_COLLECTOR_PROC_PREFIX_PATH"/proc/"
#define MEMINFO_PATH PROC_DIR"/meminfo"
#define STATUS_SIZE 7
#define PATH_DEFAULT_SIZE 250
#define LINE_SIZE 1024
#define FILE_NAME_SIZE 128

#define ME "memory"

using namespace std;
using namespace google::protobuf;
using namespace prplprobe::internal::v1;
using namespace kpi::meminfo::v1;
using namespace kpi::meminfopp::v1;

ModuleConfiguration memory_configuration;

/**
 * \brief Retrieves the cpuload of the device
 *
 * \param[out] ptr_mem_toral pointer that will be filled with the total memory
 * \param[out] ptr_mem_free pointer that will be filled with the free memory
 * \param[out] ptr_mem_available pointer that will be filled with the available memory
 * \param[out] ptr_buffers pointer that will be filled with the buffers memory
 * \param[out] ptr_cached pointer that will be filled with the cached memory
 * \param[out] ptr_process_rank pointer that will be filled with the process rank
 * \param[out] ptr_process_name pointer that will be filled with the process rank
 * \param[out] ptr_process_pid pointer that will be filled with the process pid
 * \param[out] ptr_process_used_memory pointer that will be filled with the process used memory
 * \param[out] ptr_process_memory pointer that will be filled with the process memory
 * \param[out] ptr_process_diff pointer that will be filled with the process memory
 *
 * \return -1 if the file /proc/stat could not be read, 0 otherwise
 */

static struct proc **processesLeak = NULL;
static int processesLeakSize = PATH_DEFAULT_SIZE;
static int usedLeakSize = 0;
int leakThreshold = 1;

struct proc {
    char name[128];
    int32_t pid;
    int32_t Currentram;    // Currentram is the current VMSize specififed in KibiBytes
    int32_t UsedMemory;    // Memory is VMRss specified in KibiBytes
    int32_t OldUsedMemory; // OldUsedMemory is the previous VMRss specified in KibiBytes
};

/* Common functions */

// Qsort's compare function.
static int compare(const void *a, const void *b) {
    const struct proc *p1 = *(struct proc **) a;
    const struct proc *p2 = *(struct proc **) b;

    if (p1->UsedMemory > p2->UsedMemory) {
        return -1;
    }
    else if (p1->UsedMemory == p2->UsedMemory) {
        return 0;
    }
    else {
        return 1;
    }
}

static int find_process_id_by_pid(struct proc **process, int needle, int size) {
    for (int i = 0; i < size; i++) {
        if (process[i]->pid == needle) {
            return i;
        }
    }
    return -1;
}

static int is_proc(char *name) {
    for (unsigned i = 0; i < strlen(name); i++) {
        if (!isdigit(name[i])) {
            return 0;
        }
    }
    return 1;
}

static int file_exist(char *filename) {
    struct stat buffer;
    return (stat(filename, &buffer) == 0);
}

static void reset_process(struct proc *process) {
    process->OldUsedMemory = -1;
    process->Currentram = -1;
    process->UsedMemory = -1;
    process->name[0] = '\0';
    process->pid = -1;
}

static void shift_processes_array(struct proc **processes, int used_size, int from_index) {
    for (int k = from_index; k < used_size; k++) {
        strcpy(processes[k]->name, processes[k + 1]->name);
        processes[k]->OldUsedMemory = processes[k + 1]->OldUsedMemory;
        processes[k]->Currentram = processes[k + 1]->Currentram;
        processes[k]->UsedMemory = processes[k + 1]->UsedMemory;
        processes[k]->pid = processes[k + 1]->pid;
    }
    reset_process(processes[used_size]);
}

static void clean_processes(struct proc **processes, int *p_used_size) {
    int used_size = *p_used_size;
    for (int i = 0; i < used_size; i++) {
        char path[100];
        snprintf(path, 100, PROC_DIR"/%d/status", processes[i]->pid);
        // if the process was killed, we delete it and shift the whole array
        if (!file_exist(path)) {
            used_size -= 1;
            reset_process(processes[i]);

            shift_processes_array(processes, used_size, i);
        }
    }
    *p_used_size = used_size;
}

/*
 * check_process_usage returns 0 on success
 *
 * */
static int check_process_usage(struct proc *process, char *proc_dir) {
    char status_file_name[strlen(proc_dir) + STATUS_SIZE + 1];
    strcpy(status_file_name, proc_dir);
    strcat(status_file_name, "/status");

    char statm_file_name[strlen(proc_dir) + STATUS_SIZE];
    strcpy(statm_file_name, proc_dir);
    strcat(statm_file_name, "/statm");
    FILE *status = fopen(status_file_name, "r");
    FILE *statm = fopen(statm_file_name, "r");
    int mem1 = -1, mem2 = -1, mem3 = -1, mem4 = -1, mem5 = -1, mem6 = -1, mem7 = -1;
    int result = 0;
    if ((status != NULL) && (statm != NULL)) {
        // test if the process uses memory
        int res = fscanf(statm, "%d %d %d %d %d %d %d", &mem1, &mem2, &mem3, &mem4, &mem5, &mem6, &mem7);
        if (((mem1 != 0) || (mem2 != 0) || (mem3 != 0) || (mem4 != 0) || (mem5 != 0) || (mem6 != 0) || (mem7 != 0)) && (res != EOF)) {
            char line[LINE_SIZE];
            char name[FILE_NAME_SIZE];
            /* mem1 is a number of pages and it is VMSize
             * memory must be specified in kibibytes
             * getpagesize gives the pages size in bytes */
            int memory = mem1 * (getpagesize() / 1024);
            // Parses the /proc/pid/status file by getting the first and the VmSize line and skipping the rest
            if ((fgets(line, LINE_SIZE, status) != NULL) && (sscanf(line, "Name: %s", name) != EOF) && (memory != -1)) {
                // If the current pid no longer references the same program as before
                if ((strlen(process->name) != 0) && strcmp(name, process->name)) {
                    reset_process(process);
                }
                /* set the name */
                strcpy(process->name, name);
                /* set VMRSS
                 * UsedMemory must be specified in kibibytes
                 * getpagesize gives the pages size in bytes */
                if (process->UsedMemory == -1) {
                    process->UsedMemory = mem2 * (getpagesize() / 1024);
                }
                else {
                    process->OldUsedMemory = process->UsedMemory;
                    process->UsedMemory = mem2 * (getpagesize() / 1024);
                }
                //set current ram (VMSize)
                process->Currentram = memory;
            }
            else {
                result = 1;
            }
        }
        else {
            result = 2;
        }
    }
    else {
        result = 2;
    }

    if (status != NULL) {
        fclose(status);
    }
    if (statm != NULL) {
        fclose(statm);
    }
    return result;
}

static struct proc **check_all_processes(struct proc **processes, int *p_used_size, int *p_processes_size) {
    DIR *d;
    struct dirent *dir;
    d = opendir(PROC_DIR);
    if (d == NULL) {
        return NULL;
    }

    int used_size = *p_used_size;
    int processes_size = *p_processes_size;
    while ((dir = readdir(d)) != NULL) {
        if ((dir->d_type == DT_DIR) && is_proc(dir->d_name)) {
            char proc_dir[strlen(PROC_DIR) + strlen(dir->d_name) + 1];
            strcpy(proc_dir, PROC_DIR);
            strcat(proc_dir, dir->d_name);
            int id = find_process_id_by_pid(processes, atoi(dir->d_name), used_size);
            // In case a new program was started during the last snapshot
            if (id == -1) {
                processes[used_size]->pid = atoi(dir->d_name);
                int result = check_process_usage(processes[used_size], proc_dir);
                if (result == 0) {
                    if (used_size == (processes_size - 1)) {
                        struct proc **tmp_processes = (struct proc **) realloc(processes, sizeof(struct proc *) * processes_size * 2);
                        if (tmp_processes) {
                            processes_size *= 2;
                            used_size = used_size + 1;
                            processes = tmp_processes;
                        }
                        else {
                            SAH_TRACEZ_ERROR(ME, "Cannot allocate memory for process");
                            break;
                        }

                        for (int i = used_size; i < processes_size; i++) {
                            processes[i] = (struct proc *) malloc(sizeof(struct proc));
                            if (!processes[i]) {
                                SAH_TRACEZ_ERROR(ME, "Cannot allocate memory for process %d", i);
                                // set the size of the array to the last element we succeeded to allocate
                                processes_size = i;
                                break;
                            }
                            reset_process(processes[i]);
                        }

                        if (processes_size == used_size) {
                            SAH_TRACEZ_ERROR(ME, "processes_size == used_size due to memory allocation error so we can't continue checking processes");
                            break;
                        }
                    }
                    else {
                        used_size++;
                    }
                }
            }
            else {
                check_process_usage(processes[id], proc_dir);
            }
        }
    }
    closedir(d);
    *p_used_size = used_size;
    *p_processes_size = processes_size;
    return processes;
}

/* END */

/* Leak */

EventList *getLeakEventList() {
    clean_processes(processesLeak, &usedLeakSize);

    struct proc **tmp = check_all_processes(processesLeak, &usedLeakSize, &processesLeakSize);
    if (tmp == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot browse system process list, giving up KPI report");
        return NULL;
    }
    processesLeak = tmp;

    EventList *event_list = new EventList();

    // display top leakers
    for (int i = 0; i < usedLeakSize; i++) {
        int32_t diff = processesLeak[i]->UsedMemory - processesLeak[i]->OldUsedMemory;
        /* Check Blacklist */
        if ((processesLeak[i]->OldUsedMemory != -1) && ( diff >= leakThreshold)) {
            Event *event = event_list->add_event();
            event->set_kpiname("Meminfopp");
            event->set_kpitype("Leak");
            fill_timestamp(event);

            MeminfoppLeak memoryLeak;
            memoryLeak.set_name(processesLeak[i]->name);
            memoryLeak.set_pid(processesLeak[i]->pid);
            memoryLeak.set_used(processesLeak[i]->UsedMemory);
            memoryLeak.set_diff(diff);

            Any *event_data = event->mutable_event_data();
            event_data->PackFrom(memoryLeak);
        }
    }

    return event_list;
}

/* END */

/* Top10 */

EventList *getTop10EventList() {
    EventList *event_list = NULL;
    struct proc **processesTop10 = NULL;
    int processesTop10Size = PATH_DEFAULT_SIZE;
    int topnb = 10;

    processesTop10 = (struct proc **) malloc(sizeof(struct proc *) * PATH_DEFAULT_SIZE);
    if (!processesTop10) {
        SAH_TRACEZ_ERROR(ME, "Cannot re-allocate memory for processes");
        return NULL;
    }

    for (int i = 0; i < PATH_DEFAULT_SIZE; i++) {
        processesTop10[i] = (struct proc *) malloc(sizeof(struct proc));
        if (!processesTop10[i]) {
            SAH_TRACEZ_ERROR(ME, "Cannot allocate memory for process %d", i);
            // set the size of the array to the last element we succeeded to allocate
            processesTop10Size = i;
            break;
        }
        reset_process(processesTop10[i]);
    }
    int usedTop10Size = 0;

    struct proc **tmp = check_all_processes(processesTop10, &usedTop10Size, &processesTop10Size);
    if (tmp == NULL) {
        SAH_TRACEZ_ERROR(ME, "Cannot browse system process list, giving up KPI report");
        goto cleanup;
    }
    processesTop10 = tmp;

    qsort(processesTop10, usedTop10Size, sizeof(processesTop10[0]), compare);
    // display top 10 memory consumers :
    if (usedTop10Size < 10) {
        topnb = usedTop10Size;
    }

    event_list = new EventList();

    for (int i = 0; i < topnb; i++) {
        Event *event = event_list->add_event();
        event->set_kpiname("Meminfopp");
        event->set_kpitype("Top10");
        fill_timestamp(event);

        MeminfoppTop10 memoryTop;
        memoryTop.set_rank(i + 1);
        memoryTop.set_name(processesTop10[i]->name);
        memoryTop.set_pid(processesTop10[i]->pid);
        memoryTop.set_usedmemory(processesTop10[i]->UsedMemory);
        memoryTop.set_memory(processesTop10[i]->Currentram);

        Any *event_data = event->mutable_event_data();
        event_data->PackFrom(memoryTop);
    }

cleanup:
    if (processesTop10) {
        for (int i = 0; i < processesTop10Size; i++) {
            if (processesTop10[i]) {
                free(processesTop10[i]);
            }
        }
        free(processesTop10);
    }

    return event_list;
}

/* END */

/* Global */

EventList *getGlobalEventList() {
    FILE *fileSource = fopen(MEMINFO_PATH, "r");

    if (!fileSource) {
        SAH_TRACEZ_ERROR(ME, "Unable to open file %s", MEMINFO_PATH);
        return NULL;
    }

    int MemTotal = 0;
    int MemFree = 0;
    int MemAvailable = 0;
    int Buffers = 0;
    int Cached = 0;

    int res = fscanf(fileSource, "MemTotal: %d kB\n MemFree: %d kB\n Buffers: %d kB\n Cached: %d kB\n", &MemTotal, &MemFree, &Buffers, &Cached);
    // Handling MemAvailable if present (https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=34e431b0ae398fc54ea69ff85ec700722c9da773)
    if (res == 2) {
        res = fscanf(fileSource, "MemAvailable: %d kB\n Buffers: %d kB\n Cached: %d kB\n", &MemAvailable, &Buffers, &Cached);
        if (res != 3) {
            SAH_TRACEZ_ERROR(ME, "Unable to parse /proc/meminfo");
            fclose(fileSource);
            return NULL;
        }
    }
    fclose(fileSource);

    EventList *event_list = new EventList();
    Event *event = event_list->add_event();
    event->set_kpiname("Meminfo");
    event->set_kpitype("Global");
    fill_timestamp(event);

    MeminfoGlobal global;
    global.set_memtotal(MemTotal);
    global.set_memfree(MemFree);
    global.set_buffers(Buffers);
    global.set_cached(Cached);
    global.set_memavailable(MemAvailable);

    Any *event_data = event->mutable_event_data();
    event_data->PackFrom(global);

    return event_list;
}

/* END */

void memory_start() {
    auto client = init_grpc_client();

    ProbeClientModule global(&client, &getGlobalEventList, 300, true);
    ProbeClientModule top10(&client, &getTop10EventList, 300, true);
    ProbeClientModule leak(&client, &getLeakEventList, 300, true);

    handle_event_loop();
}

void memory_cleanup() {
    SAH_TRACEZ_INFO(ME, "Module stopped.");

    if (processesLeak) {
        for (int i = 0; i < processesLeakSize; i++) {
            if (processesLeak[i]) {
                free(processesLeak[i]);
            }
        }
        free(processesLeak);
    }
}

void memory_init() {
    SAH_TRACEZ_INFO(ME, "Module started.");

    processesLeak = (struct proc **) malloc(sizeof(struct proc *) * PATH_DEFAULT_SIZE);
    if (!processesLeak) {
        SAH_TRACEZ_ERROR(ME, "Cannot allocate memory for process");
        return;
    }

    for (int i = 0; i < PATH_DEFAULT_SIZE; i++) {
        processesLeak[i] = (struct proc *) malloc(sizeof(struct proc));
        if (!processesLeak[i]) {
            SAH_TRACEZ_ERROR(ME, "Cannot allocate memory for process %d", i);
            // set the size of the array to the last element we succeeded to allocate
            processesLeakSize = i;
            break;
        }
        reset_process(processesLeak[i]);
    }
}

int memory() {
    RestoreConfiguration(memory_configuration, DEFAULT_FILE, PERSIST_FILE);
    InitSahTrace(memory_configuration.sahtrace_config());
    init_event_loop();

    memory_init();
    memory_start();

    /* Cleanup */
    memory_cleanup();
    CleanupSahTrace();
    BackupConfiguration(memory_configuration, PERSIST_FILE);

    return 0;
}
